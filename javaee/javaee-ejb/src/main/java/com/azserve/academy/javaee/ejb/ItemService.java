package com.azserve.academy.javaee.ejb;

import static com.azserve.azframework.query.Expressions.equal;
import static com.azserve.azframework.query.Expressions.like;
import static com.azserve.azframework.query.Expressions.lower;
import static com.azserve.azframework.query.Expressions.or;
import static com.azserve.azframework.query.Expressions.path;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.azserve.academy.javaee.ejb.eao.JeeEao;
import com.azserve.academy.javaee.entity.Item;
import com.azserve.academy.javaee.entity.Item_;
import com.azserve.academy.javaee.interfaces.dto.ItemDTO;
import com.azserve.academy.javaee.interfaces.qdto.ItemQDTO;
import com.azserve.academy.javaee.interfaces.service.IItemService;
import com.azserve.azframework.dto.DtoReferenceWithInteger;
import com.azserve.azframework.dto.DtoUtils;
import com.azserve.azframework.ejb.eao.ApplicationEAO;
import com.azserve.azframework.ejb.manager.EntityDtoManager;
import com.azserve.azframework.ejb.manager.EntitySecurityManager;
import com.azserve.azframework.query.Expressions;

@Stateless
@LocalBean
@Local(IItemService.class)
public class ItemService extends EntityDtoManager<Item, Integer, ItemDTO, Integer, DtoReferenceWithInteger<ItemDTO>, Serializable> implements IItemService {

	@EJB
	private JeeEao eao;

	@Override
	protected ApplicationEAO getEao() {
		return this.eao;
	}

	@Override
	protected EntitySecurityManager<Item, Serializable> createSecurityManager() {
		return EntitySecurityManager.withoutSecurity();
	}

	@Override
	protected Class<Item> getEntityClass() {
		return Item.class;
	}

	@Override
	protected Integer toId(final DtoReferenceWithInteger<ItemDTO> reference) {
		return reference == null ? null : reference.getId();
	}

	@Override
	protected ItemDTO newDto(final Item entity) {
		return new ItemDTO(entity.getId());
	}

	@Override
	protected DtoReferenceWithInteger<ItemDTO> toReference(final Integer id) {
		return new DtoReferenceWithInteger<>(id);
	}

	@Override
	protected void fillEntity(final ItemDTO dto, final Item entity) {
		entity.setCode(dto.getCode());
		entity.setDescription(dto.getDescription());
	}

	@Override
	protected void fillDto(final ItemDTO dto, final Item entity) {
		dto.setCode(entity.getCode());
		dto.setDescription(entity.getDescription());
	}

	@Override
	public ItemDTO findDto(final String code) {
		final EntityManager em = this.eao.getEntityManager();
		final CriteriaBuilder cb = em.getCriteriaBuilder();
		final CriteriaQuery<Item> cr = cb.createQuery(Item.class);
		final Root<Item> root = cr.from(Item.class);
		cr.select(root);
		cr.where(cb.equal(root.get(Item_.code), code));
		final TypedQuery<Item> query = em.createQuery(cr);
		final List<Item> results = query.getResultList();
		return results.stream().findAny().map(this::toDto).orElse(null);
	}

	@Override
	public List<ItemQDTO> findAll() {
		return this.query(null)
				.stream()
				.map(e -> new ItemQDTO(new DtoReferenceWithInteger<>(e.getId()), e.getCode(), e.getDescription()))
				.toList();
	}

	@Override
	public List<ItemQDTO> find(final String text, final int offset, final int limit) {
		if (text == null || text.length() == 0) {
			return List.of();
		}
		return this.query(null)
				.where(or(
						equal(path(Item_.code), text),
						like(lower(path(Item_.description)), "%" + text.toLowerCase() + "%")))
				.order(path(Item_.code))
				.offset(offset)
				.limit(limit)
				.stream()
				.map(i -> new ItemQDTO(DtoUtils.createReference(i.getId()), i.getCode(), i.getDescription()))
				.toList();
	}

	@Override
	public ItemQDTO find(final DtoReferenceWithInteger<ItemDTO> ref) {
		if (ref == null) {
			return null;
		}
		return this.query(null)
				.where(equal(path(Item_.id), ref.getId()))
				.limit(1)
				.stream()
				.map(i -> new ItemQDTO(DtoUtils.createReference(i.getId()), i.getCode(), i.getDescription()))
				.findAny()
				.orElse(null);
	}

	@Override
	public int count(final String text) {
		if (text == null || text.length() == 0) {
			return 0;
		}
		return this.query(null)
				.select(Long.class, Expressions.count())
				.where(or(
						equal(path(Item_.code), text),
						like(lower(path(Item_.description)), "%" + text.toLowerCase() + "%")))
				.fetchSingleResult().intValue();
	}

}
