package com.azserve.academy.javaee.ejb.eao;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.azserve.azframework.ejb.eao.ApplicationEAO;

@Stateless
@LocalBean
public class JeeEao extends ApplicationEAO {

	@PersistenceContext(unitName = "PU_javaee")
	private EntityManager em;

	@Resource
	private SessionContext sessionContext;

	@Override
	public EntityManager getEntityManager() {
		return this.em;
	}

	@Override
	public SessionContext getSessionContext() {
		return this.sessionContext;
	}

}
