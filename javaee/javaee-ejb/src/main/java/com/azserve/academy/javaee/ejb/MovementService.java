package com.azserve.academy.javaee.ejb;

import static com.azserve.azframework.query.Expressions.equal;
import static com.azserve.azframework.query.Expressions.ge;
import static com.azserve.azframework.query.Expressions.lt;
import static com.azserve.azframework.query.Expressions.opt;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.azserve.academy.javaee.ejb.eao.JeeEao;
import com.azserve.academy.javaee.entity.Item;
import com.azserve.academy.javaee.entity.Item_;
import com.azserve.academy.javaee.entity.Movement;
import com.azserve.academy.javaee.entity.Movement_;
import com.azserve.academy.javaee.interfaces.dto.MovementDTO;
import com.azserve.academy.javaee.interfaces.qdto.MovementQDTO;
import com.azserve.academy.javaee.interfaces.service.IMovementService;
import com.azserve.azframework.common.lang.StringUtils;
import com.azserve.azframework.dto.DtoReferenceWithInteger;
import com.azserve.azframework.dto.DtoUtils;
import com.azserve.azframework.ejb.eao.ApplicationEAO;
import com.azserve.azframework.ejb.manager.EntityDtoManager;
import com.azserve.azframework.ejb.manager.EntitySecurityManager;
import com.azserve.azframework.query.Expressions;
import com.azserve.azframework.query.Queries;
import com.azserve.azframework.query.criteria.IJoin;
import com.azserve.azframework.query.criteria.IPath;
import com.azserve.azframework.query.criteria.IRoot;

@Stateless
@LocalBean
@Local(IMovementService.class)
public class MovementService extends EntityDtoManager<Movement, Integer, MovementDTO, Integer, DtoReferenceWithInteger<MovementDTO>, Serializable> implements IMovementService {

	@EJB
	private JeeEao eao;

	@Override
	public List<MovementQDTO> search(final String code, final LocalDate minDate, final LocalDate maxDate) {
		final IRoot<Movement> _movement = Expressions.from(Movement.class);
		final IPath<Integer> _id = _movement.get(Movement_.id);
		final IPath<Integer> _amount = _movement.get(Movement_.amount);
		final IPath<LocalDateTime> _date = _movement.get(Movement_.date);

		final IJoin<Movement, Item> _item = _movement.join(Movement_.item);
		final IPath<Integer> _itemId = _item.get(Item_.id);
		final IPath<String> _itemCode = _item.get(Item_.code);
		final IPath<String> _itemDescription = _item.get(Item_.description);
		return Queries
				.create(this.eao.getEntityManager())
				.from(_movement)
				.selectTuple(_id, _amount, _date, _itemId, _itemCode, _itemDescription)
				.where(
						opt(StringUtils.emptyToNull(code), c -> equal(_itemCode, c)),
						opt(minDate, d -> ge(_date, d.atStartOfDay())),
						opt(maxDate, d -> lt(_date, d.atStartOfDay().plusDays(1))))
				.stream()
				.map(t -> new MovementQDTO(
						DtoUtils.createReference(t._1),
						DtoUtils.createReference(t._4),
						t._5,
						t._6,
						t._2,
						t._3))
				.toList();
	}

	@Override
	protected Integer toId(final DtoReferenceWithInteger<MovementDTO> reference) {
		return reference == null ? null : reference.getId();
	}

	@Override
	protected DtoReferenceWithInteger<MovementDTO> toReference(final Integer id) {
		return DtoUtils.createReference(id);
	}

	@Override
	protected void fillEntity(final MovementDTO dto, final Movement entity) {
		entity.setAmount(dto.getAmount());
		entity.setDate(dto.getDate());
		entity.setItem(this.eao.getReference(Item.class, dto.getItem().getId()));
	}

	@Override
	protected MovementDTO newDto(final Movement entity) {
		return new MovementDTO(entity.getId());
	}

	@Override
	protected void fillDto(final MovementDTO dto, final Movement entity) {
		dto.setAmount(entity.getAmount());
		dto.setDate(entity.getDate());
		dto.setItem(DtoUtils.createReference(entity.getItem().getId()));
	}

	@Override
	protected EntitySecurityManager<Movement, Serializable> createSecurityManager() {
		return EntitySecurityManager.withoutSecurity();
	}

	@Override
	protected Class<Movement> getEntityClass() {
		return Movement.class;
	}

	@Override
	protected ApplicationEAO getEao() {
		return this.eao;
	}

}
