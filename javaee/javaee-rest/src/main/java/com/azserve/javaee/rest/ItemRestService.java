package com.azserve.javaee.rest;

import java.util.List;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.azserve.academy.javaee.interfaces.qdto.ItemQDTO;
import com.azserve.academy.javaee.interfaces.service.IItemService;
import com.azserve.azframework.dto.DtoReferenceWithInteger;

@Path("item")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Dependent
public class ItemRestService {

	@Inject
	private IItemService itemService;

	@GET
	@Path("/{id}")
	public ItemQDTO getItem(@PathParam("id") final String id) {
		return this.itemService.find(new DtoReferenceWithInteger<>(Integer.valueOf(id)));
	}

	@GET
	@Path("")
	public List<ItemQDTO> getMovements() {
		return this.itemService.findAll();
	}
}
