package com.azserve.academy.javaee.display;

import java.util.function.Consumer;

import com.azserve.academy.javaee.interfaces.dto.ItemDTO;
import com.azserve.azframework.display.Display;

public interface ItemDisplay extends Display {

	void loadData(ItemDTO value);

	void setOnSave(Consumer<ItemDTO> callback);

}
