package com.azserve.academy.javaee.display;

import com.azserve.academy.javaee.interfaces.qdto.ItemQDTO;
import com.azserve.azframework.display.DataListDisplay;

public interface ItemDataListDisplay extends DataListDisplay<ItemQDTO> {

}
