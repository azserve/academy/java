package com.azserve.academy.javaee.display;

import com.azserve.academy.javaee.interfaces.dto.MovementDTO;
import com.azserve.azframework.display.DataDisplay;

public interface MovementDataDisplay extends DataDisplay<MovementDTO> {

}
