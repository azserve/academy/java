package com.azserve.academy.javaee.presenter;

import javax.enterprise.context.Dependent;

import com.azserve.academy.javaee.display.ItemDisplay;
import com.azserve.academy.javaee.interfaces.dto.ItemDTO;
import com.azserve.azframework.presenter.AbstractPresenter;

@Dependent
public class ItemPresenter extends AbstractPresenter<ItemDisplay> {

	ItemDTO value = new ItemDTO();
	{
		this.value.setCode("code");
		this.value.setDescription("description");
	}

	@Override
	protected void initDisplay() {
		this.getDisplay().loadData(this.value);
		this.getDisplay().setOnSave(this::save);
	}

	private void save(final ItemDTO item) {
		// save to back end
		System.out.println(item.getCode() + " " + item.getDescription());
	}

}
