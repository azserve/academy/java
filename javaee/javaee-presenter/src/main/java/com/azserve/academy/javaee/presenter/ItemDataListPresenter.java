package com.azserve.academy.javaee.presenter;

import java.util.List;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import com.azserve.academy.javaee.display.ItemDataListDisplay;
import com.azserve.academy.javaee.interfaces.qdto.ItemQDTO;
import com.azserve.academy.javaee.interfaces.service.IItemService;
import com.azserve.azframework.presenter.DataListPresenter;

@Dependent
public class ItemDataListPresenter extends DataListPresenter<ItemQDTO, ItemDataListDisplay> {

	private final IItemService itemService;

	@Inject
	public ItemDataListPresenter(final IItemService itemService) {
		this.itemService = itemService;
	}

	@Override
	protected void initDisplay() {
		this.getDisplay().setItems(this.retrieveItems());
		this.getDisplay().setOnInsert(this::insert);
	}

	@Override
	protected List<ItemQDTO> retrieveItems() {
		return this.itemService.findAll();
	}

	@Override
	protected String getItemKeyAsString(final ItemQDTO item) {
		return item.id().toString();
	}

	@Override
	protected void doDelete(final ItemQDTO item) throws Exception {
		this.itemService.delete(item.id());
	}

	private void insert() {}

}
