package com.azserve.academy.javaee.presenter;

import java.time.LocalDate;
import java.util.List;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import com.azserve.academy.javaee.display.MovementsDataListDisplay;
import com.azserve.academy.javaee.interfaces.qdto.MovementQDTO;
import com.azserve.academy.javaee.interfaces.service.IMovementService;
import com.azserve.azframework.presenter.DataListPresenter;

@Dependent
public class MovementDataListPresenter extends DataListPresenter<MovementQDTO, MovementsDataListDisplay> {

	private final IMovementService movementService;

	@Inject
	public MovementDataListPresenter(final IMovementService movementService) {
		this.movementService = movementService;
	}

	@Override
	protected void initDisplay() {
		this.getDisplay().setOnSearch(this::loadItems);
	}

	@Override
	protected List<MovementQDTO> retrieveItems() {
		final MovementsDataListDisplay display = this.getDisplay();
		final String code = display.getCode();
		final LocalDate minDate = display.getMinDate();
		final LocalDate maxDate = display.getMaxDate();
		return this.movementService.search(code, minDate, maxDate);
	}

	@Override
	protected String getItemKeyAsString(final MovementQDTO item) {
		return item.id().toString();
	}

	@Override
	protected void doDelete(final MovementQDTO item) throws Exception {
		this.movementService.delete(item.id());
	}

}
