package com.azserve.academy.javaee.display;

import com.azserve.academy.javaee.interfaces.dto.ItemDTO;
import com.azserve.azframework.display.DataDisplay;

public interface ItemDataDisplay extends DataDisplay<ItemDTO> {

}
