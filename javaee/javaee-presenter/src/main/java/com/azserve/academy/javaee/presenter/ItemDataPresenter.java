package com.azserve.academy.javaee.presenter;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import com.azserve.academy.javaee.display.ItemDataDisplay;
import com.azserve.academy.javaee.interfaces.dto.ItemDTO;
import com.azserve.academy.javaee.interfaces.service.IItemService;
import com.azserve.azframework.dto.DtoReferenceWithInteger;
import com.azserve.azframework.presenter.DataPresenter;

@Dependent
public class ItemDataPresenter extends DataPresenter<ItemDataDisplay, ItemDTO> {

	private final IItemService itemService;

	@Inject
	public ItemDataPresenter(final IItemService itemService) {
		this.itemService = itemService;
	}

	@Override
	protected ItemDTO findData(final String key) {
		return this.itemService.findDto(new DtoReferenceWithInteger<>(Integer.valueOf(key)));
	}

	@Override
	protected ItemDTO createInsertData() {
		return new ItemDTO();
	}

	@Override
	protected ItemDTO doInsert(final ItemDTO data) throws Exception {
		return this.itemService.insert(data);
	}

	@Override
	protected void doUpdate(final ItemDTO data) throws Exception {
		this.itemService.update(data);
	}

	@Override
	protected void initDisplay() {

	}
}
