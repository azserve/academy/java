package com.azserve.academy.javaee.display;

import java.time.LocalDate;

import com.azserve.academy.javaee.interfaces.qdto.MovementQDTO;
import com.azserve.azframework.display.DataListDisplay;

public interface MovementsDataListDisplay extends DataListDisplay<MovementQDTO> {

	String getCode();

	LocalDate getMinDate();

	LocalDate getMaxDate();

	void setOnSearch(Runnable callback);
}
