package com.azserve.academy.javaee.presenter;

import java.time.LocalDateTime;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import com.azserve.academy.javaee.display.MovementDataDisplay;
import com.azserve.academy.javaee.interfaces.dto.MovementDTO;
import com.azserve.academy.javaee.interfaces.service.IMovementService;
import com.azserve.azframework.dto.DtoReferenceWithInteger;
import com.azserve.azframework.presenter.DataPresenter;

@Dependent
public class MovementDataPresenter extends DataPresenter<MovementDataDisplay, MovementDTO> {

	private final IMovementService movementService;

	@Inject
	public MovementDataPresenter(final IMovementService movementService) {
		this.movementService = movementService;
	}

	@Override
	protected void initDisplay() {
		// TODO Auto-generated method stub

	}

	@Override
	protected MovementDTO findData(final String key) {
		return this.movementService.findDto(new DtoReferenceWithInteger<>(Integer.valueOf(key)));
	}

	@Override
	protected MovementDTO createInsertData() {
		final MovementDTO movementDTO = new MovementDTO();
		movementDTO.setDate(LocalDateTime.now());
		return movementDTO;
	}

	@Override
	protected MovementDTO doInsert(final MovementDTO data) throws Exception {
		return this.movementService.insert(data);
	}

	@Override
	protected void doUpdate(final MovementDTO data) throws Exception {
		this.movementService.update(data);
	}

}
