package com.azserve.academy.javaee.interfaces.service;

import java.time.LocalDate;
import java.util.List;

import com.azserve.academy.javaee.interfaces.dto.MovementDTO;
import com.azserve.academy.javaee.interfaces.qdto.MovementQDTO;
import com.azserve.azframework.dto.DtoReferenceWithInteger;

public interface IMovementService {

	List<MovementQDTO> search(String code, LocalDate minDate, LocalDate maxDate);

	MovementDTO findDto(DtoReferenceWithInteger<MovementDTO> reference);

	MovementDTO insert(MovementDTO dto) throws Exception;

	void update(MovementDTO dto) throws Exception;

	void delete(DtoReferenceWithInteger<MovementDTO> reference) throws Exception;
}
