package com.azserve.academy.javaee.interfaces.qdto;

import com.azserve.academy.javaee.interfaces.dto.ItemDTO;
import com.azserve.azframework.dto.DtoReferenceWithInteger;

public record ItemQDTO(DtoReferenceWithInteger<ItemDTO> id, String code, String description) {}
