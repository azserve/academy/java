package com.azserve.academy.javaee.interfaces.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import com.azserve.azframework.common.annotation.processing.Bean;
import com.azserve.azframework.dto.DtoReferenceWithInteger;
import com.azserve.azframework.dto.DtoWithInteger;

@Bean
public class MovementDTO extends DtoWithInteger<MovementDTO> {

	private static final long serialVersionUID = 5271401968828605179L;

	@NotNull
	private DtoReferenceWithInteger<ItemDTO> item;

	private Integer amount;

	private LocalDateTime date;

	public MovementDTO() {
		super();
	}

	public MovementDTO(final Integer id) {
		super(id);
	}

	public DtoReferenceWithInteger<ItemDTO> getItem() {
		return this.item;
	}

	public void setItem(final DtoReferenceWithInteger<ItemDTO> itemId) {
		this.item = itemId;
	}

	public Integer getAmount() {
		return this.amount;
	}

	public void setAmount(final Integer amount) {
		this.amount = amount;
	}

	public LocalDateTime getDate() {
		return this.date;
	}

	public void setDate(final LocalDateTime date) {
		this.date = date;
	}

}
