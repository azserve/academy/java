package com.azserve.academy.javaee.interfaces.dto;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.azserve.azframework.common.annotation.processing.Bean;
import com.azserve.azframework.dto.DtoWithInteger;

@Bean
public class ItemDTO extends DtoWithInteger<ItemDTO> {

	private static final long serialVersionUID = 1L;

	@Size(min = 5, max = 10)
	@Pattern(regexp = "\\d+", message = "formato non corretto, inserire solo cifre")
	private String code;

	@Size(max = 255)
	private String description;

	public ItemDTO() {
		super();
	}

	public ItemDTO(final Integer id) {
		super(id);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

}
