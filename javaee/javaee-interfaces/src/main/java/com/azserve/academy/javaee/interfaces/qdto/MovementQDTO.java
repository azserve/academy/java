package com.azserve.academy.javaee.interfaces.qdto;

import java.time.LocalDateTime;

import com.azserve.academy.javaee.interfaces.dto.ItemDTO;
import com.azserve.academy.javaee.interfaces.dto.MovementDTO;
import com.azserve.azframework.dto.DtoReferenceWithInteger;

public record MovementQDTO(
		DtoReferenceWithInteger<MovementDTO> id,
		DtoReferenceWithInteger<ItemDTO> itemId,
		String itemCode,
		String itemDescription,
		Integer amount,
		LocalDateTime date) {

}
