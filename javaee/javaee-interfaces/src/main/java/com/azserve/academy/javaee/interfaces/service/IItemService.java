package com.azserve.academy.javaee.interfaces.service;

import java.util.List;

import com.azserve.academy.javaee.interfaces.dto.ItemDTO;
import com.azserve.academy.javaee.interfaces.qdto.ItemQDTO;
import com.azserve.azframework.dto.DtoReferenceWithInteger;

public interface IItemService {

	ItemDTO findDto(DtoReferenceWithInteger<ItemDTO> reference);

	ItemDTO findDto(String code);

	ItemDTO insert(ItemDTO dto) throws Exception;

	void update(ItemDTO dto) throws Exception;

	void delete(DtoReferenceWithInteger<ItemDTO> reference) throws Exception;

	List<ItemQDTO> findAll();

	List<ItemQDTO> find(final String text, final int offset, final int limit);

	int count(String text);

	ItemQDTO find(DtoReferenceWithInteger<ItemDTO> ref);

}
