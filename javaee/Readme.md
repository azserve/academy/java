# JavaEE
Esempio di un progetto fullstack che utilizza
- Java EE 8
    - Wildfly 26.1.3
- Vaadin 23


## Wildfly
** Download **

https://github.com/wildfly/wildfly/releases/download/26.1.3.Final/wildfly-26.1.3.Final.tar.gz

** Configurazione ** 

* estrarlo
* Copiare la directory modules nella home di WF 26 per aggiungere i moduli relativi a keycloak e postgresql
* Creare il server facendolo puntare al file standalone/configuration/rv.xml
* Facendo doppio click nel server -> open launch configuration nella sezione vm argument aggiungere:

```
-Dazserve.db.url=jdbc:postgresql://<<host>>/<<db>>
-Dazserve.db.user=***
-Dazserve.db.password=***
```

## Configurazione Eclipse

** Maven **

Dal menu Window -> Preferences, cliccare su Maven -> Annotation Processing e selezione "Automatically: configure JDT APT"
