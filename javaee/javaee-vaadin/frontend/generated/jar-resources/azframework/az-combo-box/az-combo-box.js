import { ComboBoxElement } from  '@vaadin/vaadin-combo-box/src/vaadin-combo-box.js';

class AZComboBoxElement extends ComboBoxElement {

	  ready() {
	    super.ready();
	  }
	  _onKeyDown(e) {
		  if (this.opened && !e.shiftKey && e.keyCode == 9) {
			  // if the overlay is open and there is only one element then automatically select the first one
			  let arr = this.filteredItems;
			  if (arr.length == 1) {
				  this.selectedItem = arr[0];
			      this._onClosed();
			  }
		  }
		  super._onKeyDown(e);
	  }

      static get is() {
	    return 'az-combo-box';
	  }
}
customElements.define(AZComboBoxElement.is, AZComboBoxElement);