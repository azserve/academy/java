import { Details } from '@vaadin/details/vaadin-details.js';


let memoizedTemplate;

class AZDetails extends Details {

	static get is() {
		return 'az-details';
	}

	static get template() {
		if (!memoizedTemplate) {
			memoizedTemplate = super.template.cloneNode(true);
			const subStyle = document.createElement("style");
			subStyle.innerHTML = `:host([blocked]) [part="toggle"] { visibility: hidden; } :host([disable-toggle-on-summary-click]) [part="summary-content"] { cursor: default; }`;
        	const superStyle = memoizedTemplate.content.querySelector('style');
        	superStyle.parentNode.insertBefore(subStyle, superStyle);
        	const summaryEl = memoizedTemplate.content.querySelector('[part~="summary"]');
        	summaryEl.setAttribute("on-click","_onToggleClickFiltered");
		}

		return memoizedTemplate;
	}

	static get properties() {
		return {
			blocked: {
				type: Boolean,
				value: false,
				reflectToAttribute: true,
				notify: true
			},
			disableToggleOnSummaryClick: {
				type: Boolean,
				value: false,
				reflectToAttribute: true,
				notify: true
			}
		};
	}

	_onToggleClickFiltered(event) {
		if (!this.disableToggleOnSummaryClick || event.target.part == "toggle") {
			this._onToggleClick(event);
		}
	}

	_onToggleClick(event) {
		if (this.blocked) {
			return;
		}
		super._onToggleClick(event);
	}

	_onToggleKeyDown(e) {
		if (this.blocked) {
			return;
		}
		super._onToggleKeyDown(e);
	}
}


customElements.define(AZDetails.is, AZDetails);

export { AZDetails };