package com.azserve.academy.javaee.vaadin;

import java.util.ArrayList;
import java.util.List;

import com.azserve.academy.javaee.interfaces.dto.ItemDTO;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route("grid")
public class GridView extends VerticalLayout {

	private static final long serialVersionUID = 1L;

	private final Grid<ItemDTO> grid = new Grid<>(ItemDTO.class);

	List<ItemDTO> items = new ArrayList<>();

	{
		for (int i = 0; i < 10; i++) {
			final ItemDTO itemDTO = new ItemDTO();
			itemDTO.setCode("c" + i);
			itemDTO.setDescription("descr " + i);
			this.items.add(itemDTO);
		}
	}

	public GridView() {
		this.grid.setSizeFull();
		this.setSizeFull();

		this.add(this.grid);

		this.grid.removeAllColumns();

		this.grid.addColumn(ItemDTO::getCode).setHeader("Codice").setSortable(true);
		this.grid.addColumn(ItemDTO::getDescription).setHeader("Descrizione");

		this.grid.setItems(this.items);

	}
}
