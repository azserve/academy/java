package com.azserve.academy.javaee.vaadin;

import java.util.function.Consumer;

import javax.inject.Inject;

import com.azserve.academy.javaee.display.ItemDisplay;
import com.azserve.academy.javaee.interfaces.dto.ItemDTO;
import com.azserve.academy.javaee.interfaces.dto.ItemDTO_;
import com.azserve.academy.javaee.presenter.ItemPresenter;
import com.azserve.azframework.vaadinflow.data.binder.AZBinder;
import com.azserve.azframework.vaadinflow.view.AbstractView;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;

@Route("item")
public class ItemView extends AbstractView<VerticalLayout, ItemDisplay> implements ItemDisplay, JeeDefaultDisplay {

	private static final long serialVersionUID = 1L;

	private final TextField code = new TextField();

	private final TextField description = new TextField();

	private final Button save = new Button("Save");

	private final AZBinder<ItemDTO> binder = new AZBinder<>(ItemDTO.class);

	private ItemDTO item;

	@Inject
	public ItemView(final ItemPresenter presenter) {
		super(presenter);

		this.binder.bind(this.code, ItemDTO_.code);
		this.binder.bind(this.description, ItemDTO_.description);

	}

	@Override
	public void loadData(final ItemDTO value) {
		this.binder.readBean(value);
		this.item = value;
	}

	@Override
	public void setOnSave(final Consumer<ItemDTO> callback) {
		this.save.addClickListener(e -> this.save(callback));
	}

	private void save(final Consumer<ItemDTO> callback) {
		try {
			this.binder.writeBean(this.item);
			callback.accept(this.item);
		} catch (final Exception ex) {
			this.showError(ex);
		}
	}

	@Override
	protected VerticalLayout initContent() {
		final VerticalLayout layout = new VerticalLayout();
		layout.add(this.code, this.description, this.save);
		return layout;
	}

}
