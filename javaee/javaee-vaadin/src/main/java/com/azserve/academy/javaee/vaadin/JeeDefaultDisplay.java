package com.azserve.academy.javaee.vaadin;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.azserve.azframework.display.Display;
import com.azserve.azframework.exception.AzGenericException;
import com.azserve.azframework.exception.AzServiceException;
import com.azserve.azframework.vaadinflow.component.AZButton;
import com.azserve.azframework.vaadinflow.component.AZDialog;
import com.azserve.azframework.vaadinflow.component.AZFlexLayout;
import com.azserve.azframework.vaadinflow.component.AZHorizontalLayout;
import com.vaadin.flow.component.HasLabel;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.FlexComponent.JustifyContentMode;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.binder.ValidationResult;
import com.vaadin.flow.theme.lumo.LumoUtility;

public interface JeeDefaultDisplay extends Display {

	@Override
	default void notifyMessage(final String message, final Type type) {
		final Notification notification = new Notification(message);
		notification.getElement().getThemeList().add(type.name().toLowerCase());
		notification.setDuration(3000);
		notification.setPosition(Position.MIDDLE);
		notification.open();
	}

	@Override
	default void showError(final Exception ex, final Runnable callback) {
		if (ex instanceof AzServiceException) {
			final StringBuilder errorMessage = new StringBuilder();
			errorMessage.append("<span>");
			errorMessage.append("Errore </br>");
			errorMessage.append(((AzServiceException) ex).getKey().getMessageKey());
			errorMessage.append("</span>");
			final Html h = new Html(errorMessage.toString());
			JeeDefaultDisplay.notifyHtmlMessage(h, Type.ERROR, false);
		} else if (ex instanceof ValidationException) {
			final ValidationException vex = (ValidationException) ex;
			final Stream<String> fieldValidationErrors = vex.getFieldValidationErrors().stream()
					.map(s -> {
						final ValidationResult msg = s.getResult().get();
						if (s.getField() instanceof HasLabel) {
							final HasLabel c = (HasLabel) s.getField();
							if (c.getLabel() != null) {
								return "<b>" + c.getLabel() + "</b>" + msg.getErrorMessage();
							}
						}
						return msg.getErrorMessage();
					});
			final Stream<String> beanValidationErrors = vex.getBeanValidationErrors().stream().map(e -> e.getErrorMessage() + "</br>");
			final String errorMessage = "<div style='padding-bottom:0.75em'><h4 style='margin-top:0.75em;color:inherit;'>Dati non validi</h4>" + Stream.concat(fieldValidationErrors, beanValidationErrors)
					.collect(Collectors.joining("", "<div style='display:flex; flex-direction:column'>", "</div>")) + "</div>";

			final Html h = new Html(errorMessage.toString());
			JeeDefaultDisplay.notifyHtmlMessage(h, Type.ERROR);
		} else if (ex instanceof AzGenericException) {
			this.notifyMessage(((AzGenericException) ex).getMessage(), Type.ERROR);
		} else {
			this.notifyMessage("Si è verificato un errore", Type.ERROR);
		}
	}

	@Override
	default void showMessage(final String message, final Type type, final Runnable callback) {
		final Icon iconClose = VaadinIcon.CLOSE_SMALL.create();
		Icon iconAlert;
		final Span text2 = new Span(message);
		switch (type) {
			case SUCCESS:
				iconAlert = VaadinIcon.CHECK_CIRCLE.create();
				iconAlert.getStyle().set("color", "rgb(62 134 53)");
				text2.getElement().getStyle().set("color", "rgb(67 109 62)");
				break;
			case INFO:
				iconAlert = VaadinIcon.INFO_CIRCLE.create();
				iconAlert.getStyle().set("color", "rgb(43 154 243)");
				text2.getElement().getStyle().set("color", "rgb(33 91 125)");
				break;
			case WARN:
				iconAlert = VaadinIcon.WARNING.create();
				iconAlert.getStyle().set("color", "rgb(255,165,0)");
				text2.getElement().getStyle().set("color", "rgb(194, 125, 0)");
				break;
			case ERROR:
				iconAlert = VaadinIcon.EXCLAMATION_CIRCLE.create();
				iconAlert.getStyle().set("color", "rgb(201 25 11)");
				text2.getElement().getStyle().set("color", "rgb(141 43 35)");
				break;
			default:
				throw new IllegalArgumentException("Unexpected value: " + type);
		}
		final AZFlexLayout text = new AZFlexLayout(iconAlert, text2);
		text.withClassName(LumoUtility.Gap.MEDIUM);
		final AZHorizontalLayout layout = new AZHorizontalLayout(text, iconClose).withJustifyContentMode(JustifyContentMode.BETWEEN).withAlignItems(Alignment.CENTER).withWidthFull();
		final AZDialog notification = new AZDialog(layout);
		notification.setCloseOnOutsideClick(false);
		notification.addDialogCloseActionListener(e -> {
			notification.close();
			if (callback != null) {
				callback.run();
			}
		});
		notification.getElement().getThemeList().add(type.name());
		iconClose.getStyle().set("cursor", "pointer").set("color", "rgb(100, 100, 100)");
		iconClose.addClickListener(e -> {
			notification.close();
			if (callback != null) {
				callback.run();
			}
		});
		notification.open();

	}

	static void notifyHtmlMessage(final Html html, final Type type) {
		notifyHtmlMessage(html, type, true);
	}

	static void notifyHtmlMessage(final Html html, final Type type, final boolean autoClose) {
		final AZButton okBtn = new AZButton("Ok").withThemeVariants(ButtonVariant.LUMO_CONTRAST, ButtonVariant.LUMO_SMALL);
		final AZHorizontalLayout footerLayout = new AZHorizontalLayout(okBtn).withJustifyContentMode(JustifyContentMode.END).withClassName(LumoUtility.Margin.Top.SMALL);
		final Notification notification = new Notification(html, footerLayout);
		notification.getElement().getThemeList().add(type.name().toLowerCase());
		notification.setDuration(autoClose ? 5000 : 0);
		notification.setPosition(Position.MIDDLE);
		okBtn.addClickListener(e -> notification.close());
		notification.open();
	}

	default void showConfirmDialog(final String title, final String text, final Runnable confirmCallback, final Runnable cancelCallback) {
		Objects.requireNonNull(confirmCallback);

		final Div content = new Div();
		content.setWidth("100%");
		content.addClassNames(LumoUtility.TextAlignment.CENTER);
		if (title != null) {
			content.add(new H5(title));
		}
		String finalText;
		if (!text.startsWith("<p>")) {
			finalText = "<p>" + text + "</p>";
		} else {
			finalText = text;
		}
		content.add(new Html(finalText));

		final Dialog dialog = new Dialog(content);
		dialog.setCloseOnEsc(false);
		dialog.setCloseOnOutsideClick(false);

		final AZHorizontalLayout btnLayout = new AZHorizontalLayout().withAlignItems(Alignment.BASELINE).withWidthFull()
				.withJustifyContentMode(JustifyContentMode.CENTER).withClassNames(LumoUtility.Margin.Top.LARGE);
		final Button btnOk = new AZButton("Sì").withThemeVariants(ButtonVariant.LUMO_SUCCESS);
		btnOk.addClickListener(e -> {
			dialog.close();
			confirmCallback.run();
		});
		btnLayout.add(btnOk);

		if (cancelCallback != null) {
			final Button btnCancel = new AZButton("No").withThemeVariants(ButtonVariant.LUMO_ERROR);
			btnCancel.addClickListener(e -> {
				dialog.close();
				cancelCallback.run();
			});
			btnLayout.add(btnCancel);
			btnCancel.setAutofocus(true);
		} else {
			btnOk.setAutofocus(true);
		}
		dialog.add(btnLayout);
		dialog.open();
	}

}
