package com.azserve.academy.javaee.vaadin;

import java.util.Optional;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.Route;

@Route("test2-page/:id/tipo/:type")
public class Test2View extends VerticalLayout implements HasUrlParameter<String>, BeforeEnterObserver {

	private static final long serialVersionUID = 1L;

	public Test2View() {
		this.add(new Label("pagina 2"));
	}

	@Override
	public void setParameter(final BeforeEvent event, @OptionalParameter final String parameter) {
		System.out.println("parameter:" + parameter);
		final QueryParameters queryParameters = event.getLocation().getQueryParameters();
		System.out.println(queryParameters.getParameters());
	}

	@Override
	public void beforeEnter(final BeforeEnterEvent event) {
		final Optional<String> id = event.getRouteParameters().get("id");
		System.out.println(id);
		final Optional<String> type = event.getRouteParameters().get("type");
		System.out.println(type);
	}
}
