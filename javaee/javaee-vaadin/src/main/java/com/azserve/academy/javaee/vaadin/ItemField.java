package com.azserve.academy.javaee.vaadin;

import com.azserve.academy.javaee.interfaces.qdto.ItemQDTO;
import com.azserve.academy.javaee.interfaces.service.IItemService;
import com.azserve.azframework.vaadinflow.field.AZComboBox;
import com.vaadin.flow.data.provider.CallbackDataProvider;

public class ItemField extends AZComboBox<ItemQDTO> {

	private static final long serialVersionUID = 1L;

	public ItemField(final IItemService itemService, final String label) {
		super(label);
		this.setItemLabelGenerator(i -> i.code() + " - " + i.description());
		this.setItems(new CallbackDataProvider<>(s -> itemService.find(s.getFilter().orElse(""), s.getOffset(), s.getLimit()).stream(),
				s -> itemService.count(s.getFilter().orElse(""))));
	}

}
