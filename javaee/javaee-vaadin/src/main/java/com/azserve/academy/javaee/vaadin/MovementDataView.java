package com.azserve.academy.javaee.vaadin;

import javax.inject.Inject;

import com.azserve.academy.javaee.display.MovementDataDisplay;
import com.azserve.academy.javaee.interfaces.dto.MovementDTO;
import com.azserve.academy.javaee.interfaces.dto.MovementDTO_;
import com.azserve.academy.javaee.interfaces.service.IItemService;
import com.azserve.academy.javaee.presenter.MovementDataPresenter;
import com.azserve.azframework.common.funct.BooleanConsumer;
import com.azserve.azframework.presenter.DataListPresenter.DataOperation;
import com.azserve.azframework.vaadinflow.component.AZVerticalLayout;
import com.azserve.azframework.vaadinflow.field.AZDateTimePickerIT;
import com.azserve.azframework.vaadinflow.field.AZIntegerField;
import com.azserve.azframework.vaadinflow.view.DataView;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route("movement")
public class MovementDataView extends DataView<VerticalLayout, MovementDataDisplay, MovementDTO> implements MovementDataDisplay, JeeDefaultDisplay {

	private static final long serialVersionUID = 1L;

	private final AZDateTimePickerIT date = new AZDateTimePickerIT("date");
	private final AZIntegerField amount = new AZIntegerField("Amount");

	private final ItemField item;

	private final Button save = new Button("Save");

	private final IItemService itemService;

	@Inject
	public MovementDataView(final MovementDataPresenter presenter, final IItemService itemService) {
		super(presenter, MovementDTO.class);
		this.itemService = itemService;
		this.item = new ItemField(itemService, "item");
	}

	@Override
	public void setOnSave(final Runnable callback) {
		this.save.addClickListener(e -> callback.run());
	}

	@Override
	public void askUnsaved(final MovementDTO data, final DataOperation operation, final BooleanConsumer saveCallback) {
		this.showConfirmDialog("Dato non salvato",
				"L'articolo non è stato salvato, vuoi continuare?",
				() -> saveCallback.accept(true),
				() -> saveCallback.accept(false));
	}

	@Override
	protected VerticalLayout initContent() {
		this.getBinder().bind(this.date, MovementDTO_.date);
		this.getBinder().bind(this.amount, MovementDTO_.amount);
		this.getBinder().bind(this.item, MovementDTO_.item,
				i -> i == null ? null : i.id(),
				r -> this.itemService.find(r));

		return new AZVerticalLayout().withChildren(this.item, this.date, this.amount, this.save);
	}

}
