package com.azserve.academy.javaee.vaadin;

import javax.inject.Inject;

import com.azserve.academy.javaee.display.ItemDataDisplay;
import com.azserve.academy.javaee.interfaces.dto.ItemDTO;
import com.azserve.academy.javaee.interfaces.dto.ItemDTO_;
import com.azserve.academy.javaee.presenter.ItemDataPresenter;
import com.azserve.azframework.common.funct.BooleanConsumer;
import com.azserve.azframework.presenter.DataListPresenter.DataOperation;
import com.azserve.azframework.vaadinflow.view.DataView;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;

@Route("item-data")
public class ItemDataView extends DataView<VerticalLayout, ItemDataDisplay, ItemDTO> implements ItemDataDisplay, JeeDefaultDisplay {

	private static final long serialVersionUID = 1L;

	private final TextField code = new TextField();

	private final TextField description = new TextField();

	private final Button save = new Button("Save");

	@Inject
	public ItemDataView(final ItemDataPresenter presenter) {
		super(presenter, ItemDTO.class);

		this.getBinder().bind(this.code, ItemDTO_.code);
		this.getBinder().bind(this.description, ItemDTO_.description);

	}

	@Override
	protected VerticalLayout initContent() {
		final VerticalLayout layout = new VerticalLayout();
		layout.add(this.code, this.description, this.save);
		return layout;
	}

	@Override
	public void setOnSave(final Runnable callback) {
		this.save.addClickListener(e -> callback.run());
	}

	@Override
	public void askUnsaved(final ItemDTO data, final DataOperation operation, final BooleanConsumer saveCallback) {
		this.showConfirmDialog("Dato non salvato",
				"L'articolo non è stato salvato, vuoi continuare?",
				() -> saveCallback.accept(true),
				() -> saveCallback.accept(false));
	}

}
