package com.azserve.academy.javaee.vaadin;

import static com.azserve.azframework.common.funct.Functions.NULL_RUNNABLE;

import java.time.LocalDate;

import javax.inject.Inject;

import com.azserve.academy.javaee.display.MovementsDataListDisplay;
import com.azserve.academy.javaee.interfaces.qdto.MovementQDTO;
import com.azserve.academy.javaee.presenter.MovementDataListPresenter;
import com.azserve.azframework.vaadinflow.component.AZButton;
import com.azserve.azframework.vaadinflow.component.AZHorizontalLayout;
import com.azserve.azframework.vaadinflow.component.AZVerticalLayout;
import com.azserve.azframework.vaadinflow.component.grid.AZGrid;
import com.azserve.azframework.vaadinflow.field.AZDatePickerIT;
import com.azserve.azframework.vaadinflow.field.AZTextField;
import com.azserve.azframework.vaadinflow.view.DataListViewRoute;
import com.azserve.azframework.vaadinflow.view.DataView;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.LocalDateTimeRenderer;
import com.vaadin.flow.data.renderer.NumberRenderer;
import com.vaadin.flow.router.Route;

@Route("movements")
public class MovementDataListView extends DataListViewRoute<VerticalLayout, MovementsDataListDisplay, MovementQDTO> implements MovementsDataListDisplay, JeeDefaultDisplay {

	private static final long serialVersionUID = 1L;

	private final AZTextField itemCode = new AZTextField("Code");
	private final AZDatePickerIT minDate = new AZDatePickerIT("Date min");
	private final AZDatePickerIT maxDate = new AZDatePickerIT("Date max");
	private final Button search = new Button("Search", VaadinIcon.SEARCH.create());
	private final Button insert = new Button("Insert", VaadinIcon.PLUS.create());
	private final Button delete = new Button("Delete", VaadinIcon.TRASH.create());
	private Runnable editCallback = NULL_RUNNABLE;

	@Inject
	public MovementDataListView(final MovementDataListPresenter presenter) {
		super(presenter);
	}

	@Override
	public void setOnInsert(final Runnable callback) {
		this.insert.addClickListener(e -> callback.run());
	}

	@Override
	public void setOnEdit(final Runnable callback) {
		this.editCallback = callback;
	}

	@Override
	public void setOnView(final Runnable callback) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setOnDelete(final Runnable callback) {
		this.delete.addClickListener(e -> callback.run());
	}

	@Override
	public String getCode() {
		return this.itemCode.getValue();
	}

	@Override
	public LocalDate getMinDate() {
		return this.minDate.getValue();
	}

	@Override
	public LocalDate getMaxDate() {
		return this.maxDate.getValue();
	}

	@Override
	public void setOnSearch(final Runnable callback) {
		this.search.addClickListener(e -> callback.run());
	}

	@Override
	protected Class<? extends DataView<?, ?, ?>> getDataViewClass(final MovementQDTO item) {
		return MovementDataView.class;
	}

	@Override
	protected VerticalLayout initContent() {

		final AZGrid<MovementQDTO> grid = this.getGrid();
		grid.addColumn(MovementQDTO::itemCode).setHeader("Code");
		grid.addColumn(MovementQDTO::itemDescription).setHeader("Description");
		grid.addColumn(new LocalDateTimeRenderer<>(MovementQDTO::date)).setHeader("Date");
		grid.addColumn(new NumberRenderer<>(MovementQDTO::amount, "%1d")).setHeader("Quantity");

		this.addEditColumn();

		this.search.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		this.delete.addThemeVariants(ButtonVariant.LUMO_ERROR);

		return new AZVerticalLayout()
				.withSizeFull()
				.withChild(new AZHorizontalLayout(this.itemCode, this.minDate, this.maxDate, this.search))
				.withChildrenExpanded(grid)
				.withChild(new AZHorizontalLayout(this.insert, this.delete));
	}

	private void addEditColumn() {
		this.getGrid().addComponentColumn(item -> {
			return new AZButton(VaadinIcon.PENCIL.create(), e -> {
				this.getGrid().deselectAll();
				this.getGrid().select(item);
				this.editCallback.run();
			}).withThemeVariants(ButtonVariant.LUMO_ICON, ButtonVariant.LUMO_SMALL);

		}).setFlexGrow(0).setWidth("4em").setFrozenToEnd(true);
	}

}
