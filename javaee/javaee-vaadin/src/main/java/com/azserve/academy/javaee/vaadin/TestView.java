package com.azserve.academy.javaee.vaadin;

import java.util.List;

import com.azserve.azframework.vaadinflow.component.AZGridLayout;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.AnchorTarget;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@PageTitle("Pagina di test")
@Route(value = "test-page")
public class TestView extends VerticalLayout {

	private static final long serialVersionUID = 8553996321325302902L;

	private final TextField textField = new TextField();

	public TestView() {
		final Label label = new Label();
		label.setText("testo della label");

		this.textField.setLabel("textField");
		this.textField.setValue("testo del campo di testo");

		final TextArea textArea = new TextArea();
		textArea.setLabel("text area");

		this.textField.addValueChangeListener(e -> {
			if (e.isFromClient()) {
				textArea.setValue("valore cambiato dall'utente");
			} else {
				textArea.setValue("valore cambiato da codice");
			}

		});

		final IntegerField integerField = new IntegerField();
		integerField.setLabel("integer field");
		integerField.setValue(Integer.valueOf(100));

		final Checkbox checkbox = new Checkbox();
		checkbox.setLabel("checkbox");

		final DatePicker datePicker = new DatePicker("date picker");

		final ComboBox<String> comboBox = new ComboBox<>("Combo box");
		comboBox.setItems(List.of("stringa 1", "stringa 2", "stringa 3"));

		final RadioButtonGroup<String> buttonGroup = new RadioButtonGroup<>("radio button", "valore1", "valore2", "valore3");

		final Anchor anchor = new Anchor("https://www.azserve.com", "link", AnchorTarget.BLANK);

		final H1 h1 = new H1("testo in h1");

		final Span span = new Span("testo nello span");

		final Span span2 = new Span(new Text("testo"), new H3("h3"));

		final AZGridLayout azGridLayout = new AZGridLayout(1).withColumns(2, 4, 6, 8, 8).withWidthFull();
		azGridLayout.withChildren(label, this.textField, textArea)
				.withChildNewLine(integerField).withChildren(checkbox, datePicker, comboBox).withChild(buttonGroup, 2).withChildren(anchor, h1, span, span2);

		this.add(azGridLayout);
		final Button button = new Button();
		button.setText("Testo del bottone");
		this.add(button);

		button.addClickListener(e -> {
			new PopUp(this::ok, this::ko).open();

			System.out.println("Post popup");

		});

		final Button b = new Button();
		b.setText("Pagina 2");
		b.setIcon(VaadinIcon.ARROW_CIRCLE_RIGHT.create());
		this.add(b);

		b.addClickListener(e -> this.pagina2());

	}

	private void pagina2() {

		UI.getCurrent().navigate(Test2View.class);
	}

	private void ok() {
		this.textField.setValue("Ha cliccato su ok");
	}

	private void ko() {
		this.textField.setValue("Ha cliccato su ko");

	}

	private class PopUp extends Dialog {

		public PopUp(final Runnable okCallback, final Runnable koCallback) {
			final Button ok = new Button("OK");
			ok.addClickListener(e -> {
				okCallback.run();
				this.close();
			});
			this.add(ok);
			final Button ko = new Button("KO");
			ko.addClickListener(e -> {
				koCallback.run();
				this.close();
			});
			this.add(ko);
		}
	}

}
