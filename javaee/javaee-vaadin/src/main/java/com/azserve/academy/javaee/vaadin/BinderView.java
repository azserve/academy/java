package com.azserve.academy.javaee.vaadin;

import com.azserve.academy.javaee.interfaces.dto.ItemDTO;
import com.azserve.academy.javaee.interfaces.dto.ItemDTO_;
import com.azserve.azframework.vaadinflow.data.binder.AZBinder;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.router.Route;

@Route(value = "binder")
public class BinderView extends VerticalLayout {

	private static final long serialVersionUID = 1L;

	private final TextField code;
	private final TextField description;
	private final Button save;

	private final ItemDTO value;

	// private final BeanValidationBinder<ItemDTO> binder = new BeanValidationBinder<>(ItemDTO.class);

	private final AZBinder<ItemDTO> binder = new AZBinder<>(ItemDTO.class);

	{
		this.value = new ItemDTO();
		this.value.setCode("001");
		this.value.setDescription("item description");
	}

	public BinderView() {
		this.code = new TextField("Code");
		// this.code.setMaxLength(5);
		this.description = new TextField("Description");
		this.save = new Button("Save", VaadinIcon.CHECK.create());

		this.add(this.code, this.description, this.save);

		this.binder.bind(this.code, ItemDTO_.code);
		this.binder.bind(this.description, ItemDTO_.description);

		this.compileFields();

		this.save.addClickListener(e -> this.save());
	}

	private void compileFields() {
		this.binder.readBean(this.value);
	}

	private void save() {
		try {
			this.binder.writeBean(this.value);
		} catch (final ValidationException ex) {
			System.out.println(ex.getBeanValidationErrors());
		}

		System.out.println("value:" + this.value.getCode() + ", " + this.value.getDescription());
	}

}
