package com.azserve.academy.javaee.vaadin;

import static com.azserve.azframework.common.funct.Functions.NULL_RUNNABLE;

import javax.inject.Inject;

import com.azserve.academy.javaee.display.ItemDataListDisplay;
import com.azserve.academy.javaee.interfaces.qdto.ItemQDTO;
import com.azserve.academy.javaee.presenter.ItemDataListPresenter;
import com.azserve.azframework.vaadinflow.component.AZButton;
import com.azserve.azframework.vaadinflow.component.AZHorizontalLayout;
import com.azserve.azframework.vaadinflow.component.AZVerticalLayout;
import com.azserve.azframework.vaadinflow.component.grid.AZGrid;
import com.azserve.azframework.vaadinflow.view.DataListViewRoute;
import com.azserve.azframework.vaadinflow.view.DataView;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@PageTitle("Item list")
@Route(value = "item-data-list")
public class ItemDataListView extends DataListViewRoute<VerticalLayout, ItemDataListDisplay, ItemQDTO> implements ItemDataListDisplay, JeeDefaultDisplay {

	private static final long serialVersionUID = 1L;

	private Runnable editCallback = NULL_RUNNABLE;
	// private Runnable deleteCallback = NULL_RUNNABLE;

	private final Button insert = new Button("Insert");

	private final Button delete = new Button("Delete");

	@Inject
	public ItemDataListView(final ItemDataListPresenter presenter) {
		super(presenter);
	}

	@Override
	public void setOnInsert(final Runnable callback) {
		this.insert.addClickListener(e -> callback.run());
	}

	@Override
	public void setOnEdit(final Runnable callback) {
		this.editCallback = callback;
	}

	@Override
	public void setOnView(final Runnable callback) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setOnDelete(final Runnable callback) {
		this.delete.addClickListener(e -> callback.run());
	}

	@Override
	protected Class<? extends DataView<?, ?, ?>> getDataViewClass(final ItemQDTO item) {
		return ItemDataView.class;
	}

	@Override
	protected VerticalLayout initContent() {

		this.insert.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		this.delete.addThemeVariants(ButtonVariant.LUMO_ERROR);

		final AZGrid<ItemQDTO> grid = this.getGrid();
		grid.setSizeFull();
		grid.removeAllColumns();

		grid.addColumn(r -> r.code()).setHeader("Code").setWidth("50px");
		grid.addColumn(r -> r.description()).setHeader("Description").setWidth("50px");

		this.addEditColumn();

		return new AZVerticalLayout().withSizeFull().withChildrenExpanded(grid)
				.withChild(new AZHorizontalLayout(this.insert, this.delete));
	}

	private void addEditColumn() {
		this.getGrid().addComponentColumn(item -> {
			return new AZButton(VaadinIcon.PENCIL.create(), e -> {
				this.getGrid().deselectAll();
				this.getGrid().select(item);
				this.editCallback.run();
			}).withThemeVariants(ButtonVariant.LUMO_ICON, ButtonVariant.LUMO_SMALL);

		}).setFlexGrow(0).setWidth("4em").setFrozenToEnd(true);
	}
}
